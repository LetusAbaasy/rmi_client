/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmi_client;
import rmi_server.*;
import java.io.*;
import java.rmi.*;
/**
 *
 * @author M4RS
 */
public class HolaMundoCliente{
   public static void main(String args[]){
      try{
         int numPuertoRMI;
         String nombreNodo;
         InputStreamReader ent = new InputStreamReader(System.in);
         BufferedReader buf = new BufferedReader(ent);
         System.out.println("Introducir el nombre del nodo del registro RMI:");
         nombreNodo = buf.readLine();
         System.out.println("Introducir el numero de puerto de registro RMI:");
         String numPuerto = buf.readLine();
         numPuertoRMI = Integer.parseInt(numPuerto);
         String URLRegistro = "rmi://"+nombreNodo+":1099"+"/"+numPuerto;
         HolaMundoInt h = (HolaMundoInt)Naming.lookup(URLRegistro);
         System.out.println("Busqueda Completa.");
         String mensaje = h.decirHola("Pato dddd");
         System.out.println("HolaMundoCliente: "+mensaje);
      }catch (Exception e){
         System.out.println("Excepcion en HolaMundoCliente: "+e);
      }
   }
}
